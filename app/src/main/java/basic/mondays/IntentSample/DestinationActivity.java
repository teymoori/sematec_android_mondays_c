package basic.mondays.IntentSample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import basic.mondays.R;

public class DestinationActivity extends AppCompatActivity {
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);
        result = findViewById(R.id.result);

        String username = getIntent().getStringExtra("username");
        String family = getIntent().getStringExtra("family");
        Boolean isIR = getIntent().getBooleanExtra("isIranian" , false) ;
        int age = getIntent().getIntExtra("age" , 0) ;
        result.setText(username + " " + family + isIR  + " " + age);
    }

    public static void main(String[] args){
        System.out.println("hi from java");
    }

}
