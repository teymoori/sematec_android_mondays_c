package basic.mondays;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static basic.mondays.PublicMethods.toast;

public class SampleBindingActivity extends AppCompatActivity
        implements View.OnClickListener {

    private TextView title ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_binding);
        bind();
    }

    void bind(){
       title = findViewById(R.id.title) ;
    //    title.setText(R.string.my_name);

        ((TextView)findViewById(R.id.title)).setText(R.string.my_name);

//        Button register = findViewById(R.id.register) ;

//        register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                title.setText("after click on register btn");
//            }
//        });
//
//        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });

        findViewById(R.id.register).setOnClickListener(this);
        findViewById(R.id.title).setOnClickListener(this);

        findViewById(R.id.register).setOnClickListener(
                V->{
            toast(this , "testttt");
        });


        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        findViewById(R.id.register).setOnClickListener(this);
        findViewById(R.id.register).setOnClickListener(V->{

        });

//        new DatabaseHandler() {
//            @Override
//            public void insert() {
//
//            }
//        };
//        test(name -> {
//            Toast.makeText(this, name, Toast.LENGTH_SHORT).show();
//        });

    }

    void test(DatabaseHandler db){

    }


    @Override
    public void onClick(View view) {
        if( view.getId()==R.id.register){


            String titleValue = title.getText().toString() ;

            toast(this , titleValue);


//            Toast.makeText(this, titleValue, Toast.LENGTH_LONG).show();

            //code to register
        }else if (view.getId()==R.id.title){
            //code
        }
    }

    interface DatabaseHandler{
        void insert(String name);
    }

}
